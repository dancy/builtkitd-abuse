# builtkitd-abuse

A project containing attempts to abuse buildkitd when it is running
with --oci-worker-no-process-sandbox (which is required for rootless
operation).

## Usage

Run `make start-daemon` to start the rootless buildkitd.

Run `make build` to test a build that tries to abuse buildkitd and to
access resources it shouldn't have access to.  At this time the build
will succesfully be able to kill buildkitd but not leak information.

