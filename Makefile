BUILDCTL := docker run --rm \
		--entrypoint buildctl-daemonless.sh \
		--link buildkitd \
		-v $$(pwd):/src:ro \
		moby/buildkit \
		--debug \
		--addr tcp://buildkitd:1234 \
		--tlsservername ubuntu.dyn.dancysoft.com \
		--tlscacert /src/ssl/cert.pem

BUILD := $(BUILDCTL) \
		build \
		--progress plain \
		--frontend dockerfile.v0 \
		--local context=/src --local dockerfile=/src

# Try to inject a fake official debian image into
# buildkitd's cache.
build-fake-debian:
	$(BUILD) \
		--opt filename=Dockerfile.test \
		--opt "build-arg:CACHEBUSTER=$$(date)" \
		--output type=image,name=docker.io/library/debian:123


# This should fail if all is right in the world
victim:
	$(BUILD) \
		--opt filename=Dockerfile.victim

du:
	$(BUILDCTL) du

prune:
	$(BUILDCTL) prune

# build:
# 	$(BUILD) \
# 		--opt filename=Dockerfile.abuse \
# 		--opt "build-arg:CACHEBUSTER=$$(date)" \
# 		--opt "build-arg:BLOCK_MAJOR=$$(($$(stat --format '%d' $$(pwd)) >> 8))" \
# 		--opt "build-arg:BLOCK_MINOR=$$(($$(stat --format '%d' $$(pwd)) & 255))" \
# 		--output type=image,name=dancysoft/testing:latest,push=true

# start-daemon:
# 	@# xref https://github.com/moby/buildkit/blob/master/docs/rootless.md#docker
# 	docker run --rm -d --name buildkitd \
# 		--security-opt seccomp=unconfined \
# 		--security-opt apparmor=unconfined \
# 		-v $$(pwd)/ssl:/ssl:ro \
# 		-v $$(pwd)/config.json:/home/user/.docker/config.json:ro \
# 		moby/buildkit:rootless \
# 		--oci-worker-no-process-sandbox \
# 		--addr tcp://0.0.0.0:1234 --tlscert /ssl/cert.pem --tlskey /ssl/cert.key --debug


# Populate the /config.json file in the buildkitd-config volume.
setup-secrets:
	docker run --rm \
		-v buildkitd-config:/config \
		-v $$(pwd)/config.json:/source.config \
		--user root \
		--entrypoint bash \
		docker-registry.wikimedia.org/buildkitd \
		-c 'cp /source.config /config/config.json && chown buildkit: /config/config.json && ls -l /config/config.json'

start-daemon: setup-secrets
	@# xref https://github.com/moby/buildkit/blob/master/docs/rootless.md#docker
	docker run --rm -d --name buildkitd \
		--security-opt seccomp=unconfined \
		--security-opt apparmor=unconfined \
		-v $$(pwd)/ssl:/ssl:ro \
		-v buildkitd-config:/var/lib/buildkit/.docker:ro \
		docker-registry.wikimedia.org/buildkitd \
		--oci-worker-no-process-sandbox \
		--addr tcp://0.0.0.0:1234 --tlscert /ssl/cert.pem --tlskey /ssl/cert.key --debug

stop-daemon:
	-docker rm -f buildkitd
	docker volume rm buildkitd-config

restart-daemon: stop-daemon start-daemon
